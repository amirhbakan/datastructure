class node {
	constructor(data) {
		this.data = data;
		this.next = null;
	}
	
	setNext(pointer) {
		this.next = pointer;
	}
	
	getNext() {
		return this.next;
	}
	
	getData() {
		return this.data;
	}
}

class LinkedList {
	constructor () {
		this.head = null;
		this.tail = null;
		this.size = 0;
	}
	
	getSize() {
		return this.size;
	}
	
	addFirst (data) {
		let newNode = new node(data);
		newNode.setNext(this.head);
		
		if (this.head === null)
			this.tail = newNode;
			
		this.head = newNode;
		this.size ++;	
	}
	
	addLast (data) {
		let newNode = new node(data);
		if (this.head === null) {
			this.head = newNode;
			this.tail = newNode;
			this.head.next = null;
		}
		else {
			this.tail.next = newNode;
		    this.tail = newNode;
		    this.tail.next = null;
		}
		this.size ++;
	}
	
	removeFirst() {
		if (this.head === null)
			return null;
		else {
			this.head = this.head.getNext();
			this.size --;
		}
	}
	
	removeLast() {
		if (this.head === null)
			return;
		else if(this.head === this.tail) {
			this.removeFirst();
			return;
		}
			
		let newNode = this.head;
		while (newNode.next !== this.tail)
			newNode = newNode.next;
			
		this.tail = newNode;
		newNode.next = null;
	}
	
	
	remove(data) {
		if(this.head === null)
			return;
		
		let prev = null;
		let current = this.head;
		while (current.data !== data) {
			prev = current;
			current = current.next;
			if(current === null)
				return;
		}
		if(current.next === null) {
			this.removeLast();
			return;
		} else if(current === this.head) {
			this.removeFirst();
			return;
		} else {
			prev.next = current.next;
		}
	}
	
	print() {
		let pointer = this.head;
		let text = '';
		while(pointer !== null) {
			text = text + ' -> ' + pointer.getData();
			pointer = pointer.getNext();
		}
		console.log('LinkedList is:' + text);		
	}
};

let l = new LinkedList();

l.addLast('baad');
l.addLast('jizz');
l.remove('baad');
l.print();
